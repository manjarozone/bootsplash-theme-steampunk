# bootsplash-theme-steampunk
Kernel Bootsplash theme for manjaro Linux using my SteampunK manjaro Background logo

the background "logo.png" is from my display 3200x1800 so respectively you have to scale it for your display...
as well proportionally scale "spinner.gif" 
for example with Gimp (installed extension Gimc),don't overwrite it ! export as & select "As Animation" 
.... :-) .....


# Installation:
-  First install `sudo pacman -S bootsplash-systemd`
-  then download`git clone https://github.com/mar2sea/bootsplash-theme-steampunk.git`
- give permissions Allow to execute file as Program to:
- `bootsplash-steampunk.sh` & `bootsplash-packer`
- right click Open in Terminal then run: `makepkg` to create Arch package 
- append `bootsplash-steampunk` hook in the end of HOOKS string of `/etc/mkinitcpio.conf`
- add `quiet bootsplash.bootfile=bootsplash-themes/steampunk/bootsplash` into `GRUB_CMDLINE_LINUX_DEFAULT` string in `/etc/default/grub`
- run `sudo mkinitcpio -p linux419` (or linux420)
- run `sudo update-grub`
- Now on click on created file `bootsplash-theme-steampunk-0.2-2-x86_64.pkg.tar.xz` & commit
- enJOy!
.... :-)...
